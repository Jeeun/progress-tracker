chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.action === 'getHtml') {

        //获取当前页面链接中的 p 参数
        const url = new URL(window.location.href);
        const params = url.searchParams;

        //当前第几集 int
        const current = parseInt(params.get('p'));

        const listElements = document.getElementsByClassName('list-box')[0].children;


        //视频列表
        const videoList = [];

        for (let i = 0; i < listElements.length; i++) {
            const timeString = listElements[i].children[0].children[0].children[1].innerHTML;  // 05:23 、 1:23:23 、 100:23:23
            //timeString (05:23 、 1:23:23 、 100:23:23) 转换为秒数考虑小时+分钟+秒数 和 分钟+秒数
            const timeArray = timeString.split(':');
            const timeLength = timeArray.length;
            const seconds = parseInt(timeArray[timeLength - 1]) + parseInt(timeArray[timeLength - 2]) * 60 + parseInt(timeArray[timeLength - 3] || 0) * 60 * 60;
            //秒数
            videoList.push(seconds);
        }


        //已经播放过的所有视频的秒数
        const playedSeconds = videoList.slice(0, current - 1).reduce((a, b) => a + b, 0);
        //当前正在播放的视频的秒数
        const currentSeconds = videoList[current - 1];
        //剩下所有未播放视频的秒数
        const restSeconds = videoList.slice(current).reduce((a, b) => a + b, 0);


        //取三个数的百分比保留1位小数
        const playedPercent = Math.round(playedSeconds / (playedSeconds + currentSeconds + restSeconds) * 1000) / 10;
        const currentPercent = Math.round(currentSeconds / (playedSeconds + currentSeconds + restSeconds) * 1000) / 10;
        const restPercent = Math.round(restSeconds / (playedSeconds + currentSeconds + restSeconds) * 1000) / 10;


        //把三个数字转回  05:23 、 1:23:23 、 100:23:23 格式
        const playedTimeString = new Date(playedSeconds * 1000).toISOString().substr(11, 8);
        const currentTimeString = new Date(currentSeconds * 1000).toISOString().substr(11, 8);
        const restTimeString = new Date(restSeconds * 1000).toISOString().substr(11, 8);


        sendResponse({
            playedSeconds: playedSeconds,
            currentSeconds: currentSeconds,
            restSeconds: restSeconds,
            playedPercent: playedPercent,
            currentPercent: currentPercent,
            restPercent: restPercent,
            playedTimeString: playedTimeString,
            currentTimeString: currentTimeString,
            restTimeString: restTimeString
        });
    }
});
