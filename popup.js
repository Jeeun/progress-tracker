document.addEventListener('DOMContentLoaded', function () {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {action: 'getHtml'}, function (response) {


            /**
             * {
             *             playedSeconds: playedSeconds,
             *             currentSeconds: currentSeconds,
             *             restSeconds: restSeconds,
             *             playedPercent: playedPercent,
             *             currentPercent: currentPercent,
             *             restPercent: restPercent,
             *             playedTimeString: playedTimeString,
             *             currentTimeString: currentTimeString,
             *             restTimeString: restTimeString
             *         }
             */


            /*

            <!--三段进度条-->
<div class="progress" style="width: 600px ;margin-top: 50px">
    <div class="progress-bar progress-bar-played" role="progressbar" aria-valuenow="60"
         aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
        60%
    </div>
    <div class="progress-bar progress-bar-current" role="progressbar" aria-valuenow="60"
         aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
        20%
    </div>
    <div class="progress-bar progress-bar-rest" role="progressbar" aria-valuenow="60"
         aria-valuemin="0" aria-valuemax="100" style="width: 78%;">
        20%
    </div>
</div>
             */


            //设置进度条百分比
            /**
             *    "restSeconds": 62159,
             *     "playedPercent": 11.3,
             *     "currentPercent": 0.3,
             *     "restPercent": 88.4,
             *
             */
            document.getElementsByClassName('progress-bar-played')[0].style.width = response.playedPercent + '%';
            document.getElementsByClassName('progress-bar-current')[0].style.width = response.currentPercent + '%';
            document.getElementsByClassName('progress-bar-rest')[0].style.width = response.restPercent + '%';

            //设置进度条显示内容
            document.getElementsByClassName('progress-bar-played')[0].innerHTML = response.playedTimeString;
            // document.getElementsByClassName('progress-bar-current')[0].innerHTML = response.currentTimeString; //当前播放时间不需要显示
            document.getElementsByClassName('progress-bar-rest')[0].innerHTML = response.restTimeString;
        });
    });
});


