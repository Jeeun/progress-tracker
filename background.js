// background.js

// 当点击插件图标时向 Content Script 发送消息
chrome.browserAction.onClicked.addListener(function(tab) {
    chrome.tabs.sendMessage(tab.id, {message: 'getDivContent'}, function(response) {
        console.log(response);
        // 这里可以处理 Content Script 返回的 <div> 内容
        // 将结果存储到 Chrome 扩展的存储中
        chrome.storage.local.set({divContent: response}, function() {
            console.log('保存成功');
        });
    });
});
