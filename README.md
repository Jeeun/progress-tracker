# ProgressTracker

#### 介绍
一个便于查看B站分P 总进时间度的插件
目前仅支持 Blibli 分P类

#### 软件架构
Chrome插件


#### 安装教程

1.  下载代码
2.  ![输入图片说明](https://foruda.gitee.com/images/1698039401607855585/ae3c7030_971517.png "屏幕截图")
3.  选择插件目录安装

#### 使用说明

1. ![输入图片说明](https://foruda.gitee.com/images/1698039460299035021/6cdf713a_971517.png "屏幕截图")

进度条分别展示看过的总时长，正在看的总时长，和剩余未看的总时长。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



